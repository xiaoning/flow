#!/bin/bash
list=0806-2
date=0823-1
user=anabel
N=3
num=4
setupATLAS
voms-proxy-init -voms atlas
mkdir -p flow
cd flow/
rm -rf source
cp -r /afs/cern.ch/user/x/xiaoning/public/flow/source .
echo 'copying package done'
cp /afs/cern.ch/user/x/xiaoning/public/flow/runlist$list.txt .
echo 'preparing to submit for'
cat runlist$list.txt
mkdir -p build
mkdir -p run
cd build
asetup AnalysisBase 21.2.110
cmake ../source
make
source x86_64-*-opt/setup.sh
cd ..
dest=$PWD

lsetup panda
input=$dest/runlist$list.txt
#cat ~/getflow/$1_runlist.txt

linenumber=0
while IFS= read -r line; do
    if [[ $line == *"done"* ]]; then
        break
    fi
    rm -rf myGridJob$linenumber
    sed -i "s@.*alg.setProperty("N".*@  alg.setProperty("N", $N).ignore();@" $dest/source/MyAnalysis/share/ATestSubmit.cxx
    sed -i "s@.*SH::scanRucio(sh,.*@SH::scanRucio(sh, \"$line\");@" $dest/source/MyAnalysis/share/ATestSubmit.cxx
    sed -i "s@.*driver.options()->setDouble(\"nc_nFilesPerJob.*@  driver.options()->setDouble(\"nc_nFilesPerJob\",$num);@" $dest/source/MyAnalysis/share/ATestSubmit.cxx
    sed -i "s@.*driver.options()->setString(\"nc_outputSampleName\",.*@  driver.options()->setString(\"nc_outputSampleName\", \"user.$user.test-grid-$list-v$N-$date.%in:name[2]%.%in:name[3]%.%in:name[6]%\");@" $dest/source/MyAnalysis/share/ATestSubmit.cxx
    #cat $dest/source/MyAnalysis/share/ATestSubmit.cxx
    cp $dest/source/MyAnalysis/share/ATestSubmit.cxx /afs/cern.ch/user/a/anabel/public/flow/run/ATestSubmit-$user-$list-v$N-$date.cxx
    root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' "$dest"'/source/MyAnalysis/share/ATestSubmit.cxx("myGridJob'$linenumber'")'
    linenumber=$((linenumber + 1))
done <$input
cd ..
