#!/bin/bash
#./runlocal.sh PC False 0.5 ~/flow/nev.txt
cd ~/flow/build
source x86_64-slc7-gcc8-opt/setup.sh
mkdir -p ../run
cd ../run
rm -rf submitDir
cp ~/flow/source/MyAnalysis/share/ATestRun_eljob.py ~/flow/runlogs/ATestRun_eljob$1PEB_$2_$3.py
sed -i "s@.*inputFilePath = .*@inputFilePath = \'/atlasgpfs01/usatlas/data/cher97/singlesamples365678/$1PEB/\'@" ~/flow/runlogs/ATestRun_eljob$1PEB_$2_$3.py
sed -i "s@.*alg.jumpLow.*@alg.jumpLow = $2@" ~/flow/runlogs/ATestRun_eljob$1PEB_$2_$3.py
sed -i "s@.*alg.jumpBar.*@alg.jumpBar = $3@" ~/flow/runlogs/ATestRun_eljob$1PEB_$2_$3.py
chmod +x ~/flow/runlogs/ATestRun_eljob$1PEB_$2_$3.py
#cat ~/flow/source/MyAnalysis/share/ATestSubmit.cxx
now=$(date +"%T")
echo "$1 $2 $3 start time : $now" >>~/flow/time.txt
~/flow/runlogs/ATestRun_eljob$1PEB_$2_$3.py submission_dir=submitDir >~/flow/runlogs/$1PEB_$2_$3.txt
now=$(date +"%T")
echo "$1 $2 $3 end time : $now" >>~/flow/time.txt
mv submitDir/data-myOutput/*.root $1PEB_$2_$3_output.root
root -b -q -l '~/flow/countevt.c("'$1PEB_$2_$3_output'","'$4'")'
#rm -rf submitDir
cd ~/flow
