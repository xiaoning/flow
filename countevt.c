#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <fstream>
#include <vector>
#include <string>
#include <complex>
#include <TComplex.h>
#include <istream>
#include <ostream>

void countevt(std::string filename = "", std::string output = "", bool sel = true, bool grid = false)
{
    ofstream myfile(output, ios::app);
    TFile *f = TFile::Open(Form("%s.root", filename.c_str()));
    TH1F *evt_fcal = (TH1F *)f->Get("evt_fcal");
    std::string date = filename.substr(filename.rfind("/user.xiaoning.test-grid-") + 25, filename.rfind(".00") - filename.rfind("user.xiaoning.test-grid-") - 24);
    std::string run = filename.substr(filename.rfind(".00") + 3, filename.rfind(".calibration") - filename.rfind(".00") - 3);
    std::string stream = filename.substr(filename.rfind("PEB") - 2, 2);
    //cout << filename << ",total," << evt_fcal->GetEntries() << endl;
    TH1F *evt_fcal_sel;
    if (sel)
    {
        evt_fcal_sel = (TH1F *)f->Get("evt_fcal_sel");

        //cout << filename << ",selected," << evt_fcal_sel->GetEntries() << endl;
    }
    if (grid)
    {
        evt_fcal_sel = (TH1F *)f->Get("evt_fcal_npu");
        //myfile << filename << ",npu," << evt_fcal_sel->GetEntries() << endl;
        //cout << filename << ",npu," << evt_fcal_sel->GetEntries() << endl;
    }
    myfile << date << "," << run << "," << stream << "," << std::setprecision(10) << evt_fcal->GetEntries() << "," << std::setprecision(10) << evt_fcal_sel->GetEntries() << endl;
    myfile.close();
    f->Close();
}
