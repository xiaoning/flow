#!/bin/bash
#./rungrid.sh dataset N date
cd /afs/cern.ch/work/x/xiaoning/flow
#asetup --restore
#cd build
#cmake ../source
#make
source build/x86_64-slc7-gcc8-opt/setup.sh
#lsetup panda
input=/afs/cern.ch/work/x/xiaoning/flow/runlist$1.txt
cat $input

linenumber=0
while IFS= read -r line; do
    if [[ $line == *"done"* ]]; then
        break
    fi
    rm -rf myGridJob$linenumber
    sed -i "s@.*SH::scanRucio(sh,.*@SH::scanRucio(sh, \"$line\");@" /afs/cern.ch/work/x/xiaoning/flow/source/MyAnalysis/share/ATestSubmit.cxx
    sed -i "s@.* driver.options()->setString(\"nc_outputSampleName\",.*@  driver.options()->setString(\"nc_outputSampleName\", \"user.xiaoning.test-grid-$1-v$2-$3.%in:name[2]%.%in:name[3]%.%in:name[6]%\");@" /afs/cern.ch/work/x/xiaoning/flow/source/MyAnalysis/share/ATestSubmit.cxx
    #echo $line
    sed -i "@.*  alg.setProperty("N"/*@  alg.setProperty("N", $2).ignore();@" afs/cern.ch/work/x/xiaoning/flow/source/MyAnalysis/share/ATestSubmit.cxx
    cp /afs/cern.ch/work/x/xiaoning/flow/source/MyAnalysis/share/ATestSubmit.cxx /afs/cern.ch/work/x/xiaoning/flow/run/ATestSubmit$1$2$3.cxx
    #cat afs/cern.ch/work/x/xiaoning/flow/source/MyAnalysis/share/ATestSubmit.cxx
    root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' '/afs/cern.ch/work/x/xiaoning/flow/source/MyAnalysis/share/ATestSubmit.cxx("myGridJob'$linenumber'")'
    linenumber=$((linenumber + 1))
done <$input
