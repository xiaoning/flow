#!/bin/bash
dest=/atlasgpfs01/usatlas/data/cher97/user.xiaoning
cd $dest
array=("$@")
arraylength=${#array[@]}
name=''
echo "date,run,stream,total,sel" >~/flow/gridevt${array[0]}'_'${array[$((arraylength - 1))]}.csv
for ((i = 0; i < ${arraylength}; i++)); do
    input=~/flow/runlist${array[$i]}.txt
    cat ~/flow/runlist${array[$i]}.txt
    while IFS= read -r line; do
        b=${line#*data18_hi.}
        c=${b%%.merge*}
        d=${line#*AOD.}
        #echo ~/flow/gridevt${array[0]}'_'${array[$((arraylength - 1))]}.csv
        root -b -q -l '~/flow/countevt.c("'$dest/tots/user.xiaoning.test-grid-${array[$i]}.$c.$d.tot'","'/usatlas/u/cher97/flow/gridevt${array[0]}'_'${array[$((arraylength - 1))]}.csv'",false,true)'
    done <$input
done
