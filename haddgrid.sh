#!/bin/bash
#argument order N user list date user2 list2 date2
dest=/atlasgpfs01/usatlas/data/cher97/user.xiaoning
cd $dest
mkdir -p $dest/tots
mkdir -p $dest/tots/each_run
array=("$@")
arraylength=${#array[@]}
name=''
N=${array[0]}
>~/flow/v$N'_namelist.txt'

for ((i = 0; i < $(((arraylength - 1) / 3)); i++)); do
    user=${array[$((i * 3 + 1))]}
    list=${array[$((i * 3 + 2))]}
    date=${array[$((i * 3 + 3))]}
    input=~/flow/runlist$list.txt
    cat ~/flow/runlist$list.txt
    name=$name$user
    echo " "
    echo $name

    while IFS= read -r line; do
        if [[ $line == *"done"* ]]; then
            break
        fi
        b=${line#*data18_hi.}
        c=${b%%.merge*}
        d=${line#*AOD.}
        #hadd $dest/tots/user.$user.test-grid-$list-v$N-$date.$c.$d.tot.root $dest/user.$user.test-grid-$list-v$N-$date.$c.$d'_myOutput.root'/*.root
        echo $dest/tots/user.$user.test-grid-$list-v$N-$date.$c.$d.tot>>~/flow/v$N'_namelist.txt'
    done <$input
done
#echo $dest/tots/user.xiaoning.test-grid.PCPEB.tot.root
#echo $dest/tots/user.xiaoning.test-grid-*.PCPEB.*'_myOutput.root'
#echo $dest/tots/user.$name.test-grid-v$N.PCPEB.tot.root
#echo $dest/tots/user.*.test-grid-*v$N*PCPEB.*.tot.root
#echo $dest/tots/user.$name.test-grid-v$N.CCPEB.tot.root
#echo $dest/tots/user.*.test-grid-*v$N*CCPEB.*.tot.root

#hadd $dest/tots/user.$name.test-grid-v$N.PCPEB.tot.root $dest/tots/user.*.test-grid-*v$N*PCPEB.*.tot.root
#hadd $dest/tots/user.$name.test-grid-v$N.CCPEB.tot.root $dest/tots/user.*.test-grid-*v$N*CCPEB.*.tot.root

#hadd $dest/tots/user.xiaoning.test-grid.PCPEB.tot.root $dest/tots/user.xiaoning.test-grid-*PCPEB.*.tot.root
#hadd $dest/tots/user.xiaoning.test-grid.CCPEB.tot.root $dest/tots/user.xiaoning.test-grid-*CCPEB.*.tot.root
