#!/bin/bash
echo "timing info" >~/flow/time.txt
echo "nev info" >~/flow/nev.txt
./runlocal.sh PC False 0.5 ~/flow/nev.txt
./runlocal.sh CC False 0.5 ~/flow/nev.txt
for i in 5 10 15 20 25; do
    ./runlocal.sh PC True $i ~/flow/nev.txt
    ./runlocal.sh CC True $i ~/flow/nev.txt
done
