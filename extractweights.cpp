const float eta_lim = 2.7;
const int eta_N = 30;
const int phi_N = 32;

void extractweights(std::string dest = "/atlasgpfs01/usatlas/data/cher97/user.xiaoning/tots", string list = "/usatlas/u/cher97/flow/v2_namelist.txt", std::string outdest = "./source/MyAnalysis/data")
{
    cout << "Start reading in file" << endl;
    std::ifstream infile(list);
    cout << infile.is_open() << endl;
    std::string line;
    TFile *fin;
    TFile *fout;
    TH2F *trk;
    TH2F *trk_temp;
    TH1F *trk_eta;
    TH1F *trk_eta_temp;
    TH2F *weight;
    TH2F *weighted;
    // get list of run numbers
    unordered_set<int> runsets;
    while (std::getline(infile, line))
    {
        cout << line << endl;
        istringstream linestream(line);
        string tmp;
        int runnumber;
        while (std::getline(linestream, tmp, '.'))
        {
            if (tmp[0] == '0' && tmp[0] == '0')
            {
                runsets.insert(stoi(tmp.substr(2, 6)));
            }
        }
    }
    infile.close();
    // iterate over run numbers
    int count = 0;

    TCanvas *c0 = new TCanvas("c0", "c0", 500, 500);
    for (auto run : runsets)
    {
        cout << run << "-------------------------------------------------------------------------" << endl;
        count++;
        infile.open(list);
        fout = TFile::Open(Form("%s/%d_weights.root", outdest.c_str(), run), "RECREATE");
        weight = new TH2F("trk_weights", "2D Track Weights", eta_N, -eta_lim, eta_lim, phi_N, (float)-TMath::Pi(), (float)TMath::Pi());
        weighted = new TH2F("trk_weighted", "2D Track Weighted", eta_N, -eta_lim, eta_lim, phi_N, (float)-TMath::Pi(), (float)TMath::Pi());
        trk = new TH2F("trk_base", "trk", eta_N, -eta_lim, eta_lim, phi_N, (float)-TMath::Pi(), (float)TMath::Pi());
        trk_eta = new TH1F("trk_eta_base", "trk_eta", eta_N, -eta_lim, eta_lim);
        //trk_weighted = new TH2F("trk_weights", "2D track weighted dist", eta_N, -eta_lim, eta_lim, phi_N, (float)-TMath::Pi(), (float)TMath::Pi());
        while (std::getline(infile, line))
        {
            if (line.find(to_string(run)) != std::string::npos)
            {
                cout << line << endl;
                fin = TFile::Open(Form("%s/%s", dest.c_str(), line.c_str()), "READ");
                TH1::AddDirectory(kFALSE);
                trk_temp = (TH2F *)fin->Get("trk");
                cout << trk_temp->GetNbinsX() << endl;
                cout << trk_temp->GetNbinsY() << endl;
                trk_eta_temp = (TH1F *)fin->Get("trk_eta");
                trk->Add(trk_temp, 1);
                trk_eta->Add(trk_eta_temp, 1);
            }
        }
        infile.close();
        cout << "Start filling 2d weight" << endl;
        for (int i = 1; i <= eta_N; i++)
        {
            double tot = 0;
            double wt_eta = trk_eta->GetBinContent(i);
            for (int j = 1; j <= phi_N; j++)
            {
                weight->SetBinContent(i, j, wt_eta / trk->GetBinContent(i, j));
                weighted->SetBinContent(i, j, wt_eta);
            }
        }
        gStyle->SetOptStat(0);
        c0->Clear();
        trk->Draw("colz");
        trk->GetXaxis()->SetRangeUser(-eta_lim, eta_lim);
        trk->GetYaxis()->SetRangeUser(-TMath::Pi(), TMath::Pi());
        c0->SaveAs(Form("grid_out/pdfs/detectordist_%d.pdf", run));
        c0->Clear();
        weight->Draw("colz");
        weight->GetXaxis()->SetRangeUser(-eta_lim, eta_lim);
        weight->GetYaxis()->SetRangeUser(-TMath::Pi(), TMath::Pi());
        c0->SaveAs(Form("grid_out/pdfs/detectorweight_%d.pdf", run));

        weighted->Draw("colz");
        weighted->GetXaxis()->SetRangeUser(-eta_lim, eta_lim);
        weighted->GetYaxis()->SetRangeUser(-TMath::Pi(), TMath::Pi());
        c0->SaveAs(Form("grid_out/pdfs/detectorweighted_%d.pdf", run));
        fout->cd();

        weight->Write();
        fin->Close();
        fout->Close();
    }
    cout << count << endl;
}
