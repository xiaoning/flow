#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include <xAODJet/JetContainer.h>
//#include <JetCalibTools/IJetCalibrationTool.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include "xAODTruth/TruthParticleContainer.h"
#include <JetUncertainties/JetUncertaintiesTool.h>
#include "HIEventUtils/HIPileupTool.h"
#include "ZdcAnalysis/IZdcAnalysisTool.h"

// Others
#include <TTree.h>
#include <TH1.h>
#include <utility>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <fstream>
#include <vector>
#include <string>
#include <complex>
#include <TComplex.h>

const Float_t FCal_range[] = {0, 0.063719, 0.14414, 0.289595, 0.525092, 0.87541, 1.36875, 2.04651, 2.98931, 5}; // fcal_cuts options
const Float_t cms_pT_range[] = {1.0, 1.25, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0, 12.0, 14.0, 20.0, 26.0, 35.0, 45.0, 60.0, 80.0, 100.0};
const int cms_pT_N = sizeof(cms_pT_range) / sizeof(float) - 1;
const Int_t cms_ctr_range[] = {0, 5, 10, 20, 30, 40, 50, 60, 80, 100};
const int cms_ctr_N = sizeof(cms_ctr_range) / sizeof(int) - 1; //include one overflow bin for ultra peripheral events

const Float_t CENT[] = {80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
const int centN = 81;

const Double_t CENTCUT[] = {0.063719, 0.06956, 0.075838, 0.082548, 0.089723, 0.097388, 0.105619, 0.114352, 0.123657, 0.133573, 0.14414, 0.155307, 0.167193, 0.179776, 0.193096, 0.207148, 0.22199, 0.237615, 0.25407, 0.27137, 0.289595, 0.308686, 0.328744, 0.349697, 0.371561, 0.394518, 0.418573, 0.443549, 0.46959, 0.49675, 0.525092, 0.554569, 0.585275, 0.617108, 0.65018, 0.684377, 0.719896, 0.756791, 0.795018, 0.834538, 0.87541, 0.917795, 0.961609, 1.0068, 1.05367, 1.10211, 1.15214, 1.20373, 1.25693, 1.31197, 1.36875, 1.42719, 1.48744, 1.55005, 1.61434, 1.68058, 1.74932, 1.81997, 1.89316, 1.96859, 2.04651, 2.12711, 2.21002, 2.29572, 2.38468, 2.47658, 2.57162, 2.66999, 2.77237, 2.87864, 2.98931, 3.10407, 3.22397, 3.34945, 3.48077, 3.61844, 3.7635, 3.91763, 4.08137, 4.26258, 10.0};

const int myColor[] = {kBlue, kViolet, kMagenta, kPink, kOrange, kYellow, kSpring, kTeal, kCyan, kAzure, kGray, kGray + 1, kGray + 3};
const int cet[] = {0, 2, 4, 6, 8}; //selected centrality sections
const int cet_N = 5;
const double cent_bin_size = (CENT[0] - CENT[centN - 1]) / (double)(cet_N - 1);
//const int cet_c = 79;
const bool PbPb = true;
char Type[2][10] = {"pp", "PbPb"};
//const char *dataType = "WorkingDefaultpp";
//Float_t Eta_range[] = {-2.7,-2.1, -1.5, -0.9, -0.3, 0.3, 0.9, 1.5, 2.1,2.7};
//const int Eta_N = sizeof(Eta_range) / sizeof(float) - 1;

const int eta_N = 50;
const float eta_lim = 2.5;
const int phi_N = 64;
const float phi_lim = 3.2;
const int maxk = 4;
const int qnks_ref = 3;
const int spks_ref = 8;
const int qnks_both = 2;
const int spks_both = 3;

bool alt = false;
bool verbose = false;
bool report = false;

const int count_max = 1500;
const int pt_min = 50;
const int pt_max = 600;

const int max_nsub = 40;

const Float_t eta_selection = 2.5;

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis(const std::string &name, ISvcLocator *pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  void clearVector();
  //void  EventPlaneAnalysis();
  double dR(const double eta1,
            const double phi1,
            const double eta2,
            const double phi2);
  double snk(int k, double weight);
  TComplex qnk(int n, int k, double angle, double weight);
  //double dRmin_jet_muon(double eta, double phi);
  //double Trig_mindEta_ion(double eta);
  //double Trig_mindPhi_ion(double phi);
  //double dRmin_muon_jet(double eta, double phi);
  //double Trig_mindEta_tc(double eta);
  //double Trig_mindPhi_tc(double phi);
  //int    parent_classify(const xAOD::TruthParticle *theParticle);
  //std::pair <int,double> parent_classify(const xAOD::TruthParticle *theParticle);
  //std::tuple <double,double,double,double, double> TruthMatch(const xAOD::Jet* jet, TString Option);
  //std::tuple<double, double, double, int, int, double, double, double, double, int, int, int, double, double, double, double, int> get_flavor_dR(const xAOD::Jet* jet);
  //std::pair <double,double> TruthMatchMuon(const xAOD::Jet* jet);
  //std::pair <double,double> TruthMatchNeu(const xAOD::Jet* jet);

  //std::tuple<std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<int>,
  //     std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<double>,std::vector<double>,std::vector<double>,
  //     std::vector<int>> TruthMuonInfo(const xAOD::Jet* jet);

  //std::tuple<std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<int>,
  //     std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<double>,std::vector<double>,std::vector<double>,
  //           std::vector<int>> TruthMuonCInfo(const xAOD::Jet* jet);

  //std::tuple<double, double, double, double, double> TrackJetMatch(const xAOD::Jet* jet);

  //std::vector<const xAOD::TruthParticle* > getDaughter(const xAOD::TruthParticle *theParticle);
  //std::vector<const xAOD::TruthParticle* > getBparent(const xAOD::TruthParticle *theParticle);

  //bool GetFJR(xAOD::Jet* jet, InDet::InDetTrackSelectionTool *m_track_selection_tool);
  //GSC calibration
  //StatusCode loadGSC();
  //StatusCode applyGSC(xAOD::JetFourMom_t *calibJet, float detEta, float thisTile0, float thisEM3, float thisNtrk, float thisWtrk, float thisCF, float thisNseg, int centrality, double *correction_fact);
  //enum GSCType {NoGSCType, Tile0, EM3, Ntrk, Wtrk, CF, Nseg};
  //std::vector< std::string  > splitLine( std::string inLine, char sep/*=','*/ );

private:
  // Configuration parameters
  bool m_isMC = false;
  bool m_isCov = false;  //whether to have covariance matrices
  bool m_minbias = true; //which trigger
  bool m_jL = false;     //skip events with no pT greater than jump bar
  double m_jB = 20.;     //min pT for skipping GeV
  bool verbose = false;
  double m_refeta = 2.5;
  double m_poieta = 2.5;
  double m_sr = 0.4;    //sampling range of corr2k
  double m_srs = 0.;    //sampling range of corr2k, shifts
  double m_res = 0.004; //resolution
  int m_nsubs = 0;      //0 for no subsamples

  int m_N = 2;

  // Tools
  //GRL
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  // Trigger decision Tool
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;  //!
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!
  // MuonSelectionTool
  //asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; //!
  // MuonCalibrationAndSmearing
  //asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool; //!
  // ID Tracking Selection Tool
  asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelection; //!
  //asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelection_sys; //!
  asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelection_pup; //!
  // Trigger Matching Tool
  //asg::AnaToolHandle<Trig::IMatchingTool> m_tmt; //!
  // Pileup Tool
  asg::AnaToolHandle<HI::HIPileupTool> m_hiPileup; //!
  // ZDC Tool
  asg::AnaToolHandle<ZDC::IZdcAnalysisTool> m_zdcAnalysisTool; //!

  //for calq
  int cent_N = PbPb ? cet_N : 1;

  //Float_t etabins[eta_N + 1][max_nsub + 1];
  //Float_t phibins[phi_N + 1][max_nsub + 1];

  std::string m_outputName;
  std::string m_outputHist;
  float m_mcSumWight = 0;
  //TTree *m_tree; //!
  TH1F *m_th1; //!

  //new TH1F for calculating corr_2k
  TComplex a;
  TComplex I;
  TComplex R_1;
  TComplex qnk_pt_poi[cms_pT_N];
  Double_t spk_pt_poi[cms_pT_N];
  TComplex qnk_pt_ref[qnks_ref];
  Double_t spk_pt_ref[spks_ref];
  TComplex qnk_pt_both[cms_pT_N][qnks_both];
  Double_t spk_pt_both[cms_pT_N][spks_both];
  //integrated reference corrn
  Double_t c_den_pt_ref[maxk / 2];
  Double_t c_wt_pt_ref[maxk / 2];
  //differential corrn
  Double_t c_den_pt_diff[maxk / 2][cms_pT_N];
  Double_t c_wt_pt_diff[maxk / 2][cms_pT_N];
  Int_t n_trks_poi[cms_pT_N];
  Int_t n_trks_ref;

  int k = 1;
  int p = 1;
  int N_ = m_N;
  int pos1 = -1;
  int pos2 = -1;
  int pos3 = -1;
  //nested loop
  //reference flow
  /*TComplex r2;
  Double_t f2wt;
  TComplex r4;
  Double_t f4wt;
  TComplex b2[cms_pT_N];
  TComplex b2_gap[cms_pT_N]; //impose 1.67 eta gap (a|c + c|a)
  TComplex b4[cms_pT_N];
  TComplex b4_2[cms_pT_N];       //testing aba'c when subver is on
  Double_t evtwt2[cms_pT_N];     // if subver, subevent
  Double_t evtwt4[cms_pT_N];     // else standard
  Double_t evtwt2_gap[cms_pT_N]; //nested loop, only gap, no subevent*/

//weights for tracks
std::string m_trkWeightsName = "MyAnalysis/365678_weights.root";
  TH2F* trk_wt;
  int trk_wt_eta_bin;
  int trk_wt_phi_bin;
  float trk_eta_lim_low;
  float trk_phi_lim_low;
  float trk_eta_lim_high;
  float trk_phi_lim_high;
  float trk_eta_width;
  float trk_phi_width;

  TH1F *evt_fcal[max_nsub + 1];
  TH1F *evt_fcal_sel[max_nsub + 1];
  TH1F *evt_fcal_pu[max_nsub + 1];
  TH1F *evt_fcal_npu[max_nsub + 1];
  TH1F *trk_eta[max_nsub + 1];
  TH1F *trk_phi[max_nsub + 1];

  //centrality 1D distributions
  TH1F *trk_etas[cms_ctr_N][max_nsub + 1];
  TH1F *trk_phis[cms_ctr_N][max_nsub + 1];

  //Track heatmaps
  //integrated track acceptance heatmap
  TH2F *trk[max_nsub + 1];
  TH2F *highst_pt_ctrl[max_nsub + 1];

  //centrality track acceptance heatmap
  TH2F *trks[cms_ctr_N][max_nsub + 1];

  //Integrated 1D Track pT distribution
  TH1F *trk_pt[max_nsub + 1];
  TH1F *highst_trk_pt[max_nsub + 1]; //highest track pt of an event;

  //centrality track 1D pT distribution
  TH1F *trk_pts[cms_ctr_N][max_nsub + 1];

  int m_EventNumber; //!
  //int m_RunNumber;       //!
  //int m_LumiBlock;       //!
  //int m_mcEventNumber;   //!
  //int m_mcChannelNumber; //!
  //float m_mcEventWeight; //!

  // pileup
  int m_is_pileup;
  //int m_is_oo_pileup;
  std::string auxSuffix = "RP";

  //covriance histograms
  //maxk=4, we have c2,c4,d2,d4
  //create an index array and variable names;
  TH1F *cs[maxk / 2][cms_ctr_N][max_nsub + 1];
  TH1F *c_diffs[maxk / 2][cms_pT_N][cms_ctr_N][max_nsub + 1];
  //index rule: first maxk/2, integrated, second half, differential
  //v2_2(pt)=corr2'/sqrt(corr2)
  TH1F *cov[maxk][maxk][cms_pT_N][cms_ctr_N];
  //covariance matrices are only for fullsamples

  std::string var1; //"c" for the first half, "d" for the second half
  std::string var2;

  double m_FCal_Et; //!


};

#endif
