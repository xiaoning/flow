#include <AsgTools/MessageCheck.h>
//#include <EventLoop/Job.h>
//#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
//#include "EventInfo/PileUpEventInfo.h"
//#include "EventInfo/EventInfo.h"
#include <TSystem.h>
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include <xAODMuon/MuonContainer.h>
#include "xAODTracking/TrackParticleContainer.h"
#include <TFile.h>
#include <PATInterfaces/CorrectionCode.h> // to check the return correction code status of tools
#include <xAODCore/ShallowAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include <PathResolver/PathResolver.h>
#include <utility>
#include <TF1.h>
// #include "HIEventUtils/HIPileupTool.h"

MyxAODAnalysis ::MyxAODAnalysis(const std::string &name,
                                ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator),
      m_grl("GoodRunsListSelectionTool/grl", this),
      m_trigDecisionTool("Trig::TrigDecisionTool/TrigDecisionTool"),
      m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool"),
      //m_muonSelection("CP::MuonSelectionTool", this),
      //m_muonCalibrationAndSmearingTool("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool", this),
      m_trkSelection("InDet::InDetTrackSelectionTool/MyTrackTool", this),
      //m_trkSelection_sys("InDet::InDetTrackSelectionTool/MyTrackTool_sys", this),
      m_trkSelection_pup("InDet::InDetTrackSelectionTool/MyTrackTool_pup", this),
      //m_tmt("Trig::MatchingTool/MyMatchingTool", this),
      m_hiPileup("HI::HIPileupTool/MyPileupTool", this),
      m_zdcAnalysisTool("ZDC::ZdcAnalysisTool/ZdcAnalysisTool", this)
{
  m_grl.declarePropertyFor(this, "grlTool");
  declareProperty("isMC", m_isMC);
  //declareProperty("MBias", m_minbias);
  declareProperty("N", m_N);
  declareProperty("Verbose", verbose);
  declareProperty("RefEta", m_refeta);
  declareProperty("PoiEta", m_poieta);
  declareProperty("isCov", m_isCov);
  declareProperty("jumpLow", m_jL);
  declareProperty("jumpBar", m_jB);
  declareProperty("numSubs", m_nsubs);
  declareProperty("trkWeights", m_trkWeightsName);
}

StatusCode MyxAODAnalysis ::initialize()
{
  //time_t my_time1 = time(NULL);

  // ctime() used to give the present time
  //printf("%s", ctime(&my_time1));
  ANA_MSG_INFO("in initialize");

  TFile *outputFile = wk()->getOutputFile("myOutput");
  m_th1 = new TH1F("m_th1", "m_th1", 1, 0, 1);
  m_th1->SetDirectory(outputFile);
  /*for (int i = 0; i < (phi_N + 1); i++)
  {
    phibins = -TMath::Pi() + i * 2. * TMath::Pi() / phi_N;
    //cout << phibins[i] << endl;
  }
  for (int i = 0; i < (eta_N + 1); i++)
  {
    etabins = -eta_lim + i * 2. * eta_lim / eta_N;
    //cout << etabins[i] << endl;
  }*/
  a = TComplex(0., 0.);
  I = TComplex(0., 1.);
  R_1 = TComplex(1., 0.);
  std::string name;
  for (int subsam = 0; subsam < m_nsubs + 1; subsam++)
  {
    if (subsam == 0)
    {
      name = "";
    }
    else
    {
      name = "_" + std::to_string(subsam);
    }
    evt_fcal[subsam] = new TH1F(Form("evt_fcal%s_v%d_ref%.1fpoi%.1f_jb%.1f", name.c_str(), m_N, m_refeta, m_poieta, m_jB), "Event FCal Distribution", 140, 0, 7.);
    evt_fcal[subsam]->SetDirectory(outputFile);
    evt_fcal[subsam]->Sumw2();
    evt_fcal_sel[subsam] = new TH1F(Form("evt_fcal_sel%s", name.c_str()), "Event FCal Distribution Selection", 140, 0, 7.);
    evt_fcal_sel[subsam]->SetDirectory(outputFile);
    evt_fcal_sel[subsam]->Sumw2();
    trk_eta[subsam] = new TH1F(Form("trk_eta%s", name.c_str()), "Integrated Track Distribution vs #eta", eta_N, -eta_lim, eta_lim);
    trk_eta[subsam]->Sumw2();
    trk_eta[subsam]->SetDirectory(outputFile);
    trk_phi[subsam] = new TH1F(Form("trk_phi%s", name.c_str()), "Integrated Track Distribution vs #eta", phi_N, -TMath::Pi(), TMath::Pi());
    trk_phi[subsam]->Sumw2();
    trk_phi[subsam]->SetDirectory(outputFile);
    trk_pt[subsam] = new TH1F(Form("trk_pt%s", name.c_str()), "Integrated Track Distribution in p_{T}", 200, 0, 100);
    trk_pt[subsam]->Sumw2();
    trk_pt[subsam]->SetDirectory(outputFile);
    highst_trk_pt[subsam] = new TH1F(Form("highst_trk_pt%s", name.c_str()), "Highest pT Track Distribution in p_{T}", 200, 0, 100);
    highst_trk_pt[subsam]->Sumw2();
    highst_trk_pt[subsam]->SetDirectory(outputFile);

    trk[subsam] = new TH2F(Form("trk%s", name.c_str()), "Integrated Track Acceptance in #eta and #phi", eta_N, -eta_lim, eta_lim, phi_N, -TMath::Pi(), TMath::Pi());
    trk[subsam]->Sumw2();
    trk[subsam]->SetDirectory(outputFile);
    highst_pt_ctrl[subsam] = new TH2F(Form("highst_pt_ctrl%s", name.c_str()), "Distribution in centrality x pT", 81, 0., 81., 200, 0., 100.);
    highst_pt_ctrl[subsam]->Sumw2();
    highst_pt_ctrl[subsam]->SetDirectory(outputFile);

    evt_fcal_pu[subsam] = new TH1F(Form("evt_fcal_pu%s", name.c_str()), "Fcal Distribution for In-time Pile-up Events", 140, 0., 7.);
    evt_fcal_pu[subsam]->SetDirectory(outputFile);
    evt_fcal_pu[subsam]->Sumw2();
    evt_fcal_npu[subsam] = new TH1F(Form("evt_fcal_npu%s", name.c_str()), "Fcal Distribution for NON In-time Pile-up Events", 140, 0., 7.);
    evt_fcal_npu[subsam]->SetDirectory(outputFile);
    evt_fcal_npu[subsam]->Sumw2();
    //TH1F* oot_pu = hotTH1F("oot_pu", "Out-of-time PileUp", 2,-0.5,1.5, "1 for PU & 0 for no PU", "Event Counts", kBlack, 0.3, 21, 1, true);
  }

  m_is_pileup = 0;
  for (int i = 0; i < cms_ctr_N; i++)
  {
    for (int subsam = 0; subsam < m_nsubs + 1; subsam++)
    {
      if (subsam == 0)
      {
        name = "";
      }
      else
      {
        name = "_" + std::to_string(subsam);
      }
      trk_etas[i][subsam] = new TH1F(Form("trk_eta_%d%s", i, name.c_str()), "Integrated Track Distribution vs #eta", eta_N, -eta_lim, eta_lim);
      trk_etas[i][subsam]->SetDirectory(outputFile);
      trk_etas[i][subsam]->Sumw2();
      trk_phis[i][subsam] = new TH1F(Form("trk_phi_%d%s", i, name.c_str()), "Integrated Track Distribution vs #phi", phi_N, -TMath::Pi(), TMath::Pi());
      trk_phis[i][subsam]->SetDirectory(outputFile);
      trk_phis[i][subsam]->Sumw2();
      trks[i][subsam] = new TH2F(Form("trk_%d%s", i, name.c_str()), "Integrated Track Acceptance in #eta and #phi", eta_N, -eta_lim, eta_lim, phi_N, -TMath::Pi(), TMath::Pi());
      trks[i][subsam]->SetDirectory(outputFile);
      trks[i][subsam]->Sumw2();
      trk_pts[i][subsam] = new TH1F(Form("trk_pt_%d%s", i, name.c_str()), "Integrated Track Distribution vs p_{T}", 200, 0, 100);
      trk_pts[i][subsam]->SetDirectory(outputFile);
      trk_pts[i][subsam]->Sumw2();
    }
  }

  //initialize correlation histograms
  int nbins = (int)(2. * m_sr / m_res);
  ANA_MSG_INFO("Number of bins: " << nbins);
  for (int c = 0; c < cms_ctr_N; c++)
  {
    for (int pt = 0; pt < cms_pT_N; pt++)
    {
      outputFile->mkdir(Form("%d-%d%%_/pt_%.1f_%.1f_GeV", cms_ctr_range[c], cms_ctr_range[c + 1], cms_pT_range[pt], cms_pT_range[pt + 1]));
      for (int halfk = 0; halfk < maxk / 2; halfk++)
      {
        for (int subsam = 0; subsam < m_nsubs + 1; subsam++)
        {
          if (subsam == 0)
          {
            name = "";
          }
          else
          {
            name = "_" + std::to_string(subsam);
          }
          outputFile->cd(Form("%d-%d%%_/pt_%.1f_%.1f_GeV", cms_ctr_range[c], cms_ctr_range[c + 1], cms_pT_range[pt], cms_pT_range[pt + 1]));
          c_diffs[halfk][pt][c][subsam] = new TH1F(Form("d%d_cent%d_pt%d%s", (halfk + 1) * 2, c, pt, name.c_str()), Form("Distribution of Differential c{%d} for Centrality %d %% to %d %% %.1f to %.1f GeV", 2 * (halfk + 1), cms_ctr_range[c], cms_ctr_range[c + 1], cms_pT_range[pt], cms_pT_range[pt + 1]), nbins, -m_sr + m_srs, m_sr + m_srs);
          c_diffs[halfk][pt][c][subsam]->Sumw2();
          c_diffs[halfk][pt][c][subsam]->SetDirectory(gDirectory);
          if (pt == 0)
          {
            outputFile->cd(Form("%d-%d%%_", cms_ctr_range[c], cms_ctr_range[c + 1]));
            cs[halfk][c][subsam] = new TH1F(Form("c%d_cent%d%s", 2 * (halfk + 1), c, name.c_str()), Form("Distribution of Integrated c{%d} for Centrality %d %% to %d %% 0.5-3.0 GeV", 2 * (halfk + 1), cms_ctr_range[c], cms_ctr_range[c + 1]), nbins, -m_sr + m_srs, m_sr + m_srs);
            cs[halfk][c][subsam]->SetDirectory(gDirectory);
            cs[halfk][c][subsam]->Sumw2();
          }
        }
      }

      //initialize for covariance matrices
      if (m_isCov)
      {
        for (int co = 0; co < (maxk - 1); co++)
        {
          var1 = (co > (maxk / 2 - 1)) ? "d" + std::to_string((co + 1 - maxk / 2) * 2) : "c" + std::to_string((co + 1) * 2);
          for (int co2 = co + 1; co2 < maxk; co2++)
          {
            var2 = (co2 > (maxk / 2 - 1)) ? "d" + std::to_string((co2 + 1 - maxk / 2) * 2) : "c" + std::to_string((co2 + 1) * 2);
            if (co == 0 && co2 == 1 && pt == 0) //c2c4
            {
              outputFile->cd(Form("%d-%d%%_", cms_ctr_range[c], cms_ctr_range[c + 1]));
              cov[co][co2][pt][c] = new TH1F(Form("cov%s%s_cent%d", var1.c_str(), var2.c_str(), c), Form("Distribution of %s*%s for Centrality %d %% to %d %%", var1.c_str(), var2.c_str(), cms_ctr_range[c], cms_ctr_range[c + 1]), nbins, -m_sr + m_srs, m_sr + m_srs);
              cov[co][co2][pt][c]->SetDirectory(gDirectory);
              cov[co][co2][pt][c]->Sumw2();
            }
            else
            {
              outputFile->cd(Form("%d-%d%%_/pt_%.1f_%.1f_GeV", cms_ctr_range[c], cms_ctr_range[c + 1], cms_pT_range[pt], cms_pT_range[pt + 1]));
              cov[co][co2][pt][c] = new TH1F(Form("cov%s%s_cent%d_pt%d", var1.c_str(), var2.c_str(), c, pt), Form("Distribution of %s*%s for Centrality %d %% to %d %%", var1.c_str(), var2.c_str(), cms_ctr_range[c], cms_ctr_range[c + 1]), nbins, -m_sr + m_srs, m_sr + m_srs);
              cov[co][co2][pt][c]->SetDirectory(gDirectory);
              cov[co][co2][pt][c]->Sumw2();
            }
          }
        }
      }
    }
  }

  /*m_tree = new TTree("tree", "tree");
  m_tree->SetDirectory(outputFile);
  m_tree->Branch("EventNumber", &m_EventNumber);
  m_tree->Branch("RunNumber", &m_RunNumber);
  m_tree->Branch("LumiBlock", &m_LumiBlock);
  m_tree->Branch("Vtx_x", &m_Vtx_x);
  m_tree->Branch("Vtx_y", &m_Vtx_y);
  m_tree->Branch("Vtx_z", &m_Vtx_z);
  // only MC

  if (m_isMC)
  {
    m_tree->Branch("mcEventNumber", &m_mcEventNumber);
    m_tree->Branch("mcChannelNumber", &m_mcChannelNumber);
    m_tree->Branch("mcEventWeight", &m_mcEventWeight);
  }

  m_tree->Branch("Fcal", (m_FCal_Et);
  m_tree->Branch("TriggerObject_Chain", &m_TriggerObject_Chain);
  m_tree->Branch("TriggerObject_Ps", &m_TriggerObject_Ps);
  m_tree->Branch("Is_pileup", &m_is_pileup);
  m_tree->Branch("Is_oo_pileup", &m_is_oo_pileup);
  // FLOW parameters
  m_tree->Branch("Qx_raw", &FCal_Qx);
  m_tree->Branch("Qy_raw", &FCal_Qy);
  m_tree->Branch("FCalSumEt", &FCal_Et);
  m_tree->Branch("Qx_raw_N", &FCal_Qx_N);
  m_tree->Branch("Qy_raw_N", &FCal_Qy_N);
  m_tree->Branch("FCalSumEt_N", &FCal_Et_N);
  m_tree->Branch("Qx_raw_P", &FCal_Qx_P);
  m_tree->Branch("Qy_raw_P", &FCal_Qy_P);
  m_tree->Branch("FCalSumEt_P", &FCal_Et_P);

  // TRACKS
    m_tree->Branch("Track_pt", &m_Track_pt);
    m_tree->Branch("Track_eta", &m_Track_eta);
    m_tree->Branch("Track_phi", &m_Track_phi);
    m_tree->Branch("Track_charge", &m_Track_charge);
    m_tree->Branch("Track_d0", &m_Track_d0);
    m_tree->Branch("Track_z0", &m_Track_z0);
    m_tree->Branch("Track_theta", &m_Track_theta);*/

  std::string GRLFilePath2017 = PathResolverFindCalibFile("MyAnalysis/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml");
  //std::string GRLFilePath2018 = PathResolverFindCalibFile("MyAnalysis/data18_hi.periodAllYear_DetStatus-v104-pro22-08_Unknown_PHYS_HeavyIonP_All_Good.xml");
  std::string GRLFilePath2018 = PathResolverFindCalibFile("MyAnalysis/data18_hi.periodAllYear_DetStatus-v104-pro22-08_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml");
  //const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  std::vector<std::string> vecStringGRL;
  //vecStringGRL.push_back(fullGRLFilePath);
  vecStringGRL.push_back(GRLFilePath2017);
  vecStringGRL.push_back(GRLFilePath2018);
  ANA_CHECK(m_grl.setProperty("GoodRunsListVec", vecStringGRL));
  ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK(m_grl.initialize());

  if (!m_isMC)
  {
    ANA_CHECK(m_trigConfigTool.initialize());
    ANA_CHECK(m_trigDecisionTool.setProperty("ConfigTool", m_trigConfigTool.getHandle()));
    ANA_CHECK(m_trigDecisionTool.setProperty("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK(m_trigDecisionTool.initialize());

    //ANA_CHECK(m_tmt.setProperty("TrigDecisionTool", m_trigDecisionTool.getHandle()));
    //ANA_CHECK(m_tmt.initialize());
  }

  /*ANA_CHECK(m_muonSelection.setProperty("TrtCutOff", true));
  ANA_CHECK(m_muonSelection.setProperty("MaxEta", 2.5));
  ANA_CHECK(m_muonSelection.setProperty("MuQuality", 0));
  ANA_CHECK(m_muonSelection.initialize());

  ANA_CHECK(m_muonCalibrationAndSmearingTool.initialize());*/
  // track selection quality
  ANA_CHECK(m_trkSelection.setProperty("CutLevel", "HITight"));
  ANA_CHECK(m_trkSelection.setProperty("minPt", 500.));
  ANA_CHECK(m_trkSelection.initialize());

  /*ANA_CHECK(m_trkSelection_sys.setProperty("CutLevel", "TightPrimary"));
  ANA_CHECK(m_trkSelection_sys.setProperty("maxZ0SinTheta", 1.0));
  ANA_CHECK(m_trkSelection_sys.setProperty("minPt", 3000.));
  ANA_CHECK(m_trkSelection_sys.setProperty("maxNSiSharedModules", 100));
  ANA_CHECK(m_trkSelection_sys.setProperty("maxD0overSigmaD0", 3.0));
  ANA_CHECK(m_trkSelection_sys.setProperty("maxZ0SinThetaoverSigmaZ0SinTheta", 3.0));
  ANA_CHECK(m_trkSelection_sys.initialize());*/

  if (!m_isMC)
  {
    ANA_CHECK(m_hiPileup.setProperty("Year", "2018"));
    ANA_CHECK(m_hiPileup.initialize());

    ANA_CHECK(m_zdcAnalysisTool.setProperty("FlipEMDelay", false));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("LowGainOnly", false));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("DoCalib", true));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("Configuration", "PbPb2018"));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("AuxSuffix", "RP"));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("ForceCalibRun", -1));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("DoTrigEff", false));
    ANA_CHECK(m_zdcAnalysisTool.setProperty("DoTimeCalib", false));

    ANA_CHECK(m_zdcAnalysisTool.initialize());
  }

  ANA_CHECK(m_trkSelection_pup.setProperty("CutLevel", "HITight"));
  ANA_CHECK(m_trkSelection_pup.setProperty("minPt", 500.));
  ANA_CHECK(m_trkSelection_pup.initialize());

//track weights
std::unique_ptr<TFile> trk_Weight_file(TFile::Open(PathResolverFindCalibFile(m_trkWeightsName).c_str(), "READ"));
trk_wt = (TH2F*)trk_Weight_file->Get("trk_weights");
trk_wt->SetDirectory(0);

  trk_wt_eta_bin = trk_wt->GetNbinsX();
  trk_wt_phi_bin = trk_wt->GetNbinsY();
  trk_eta_lim_low = trk_wt->GetXaxis()->GetBinLowEdge(1);
  trk_phi_lim_low = trk_wt->GetYaxis()->GetBinLowEdge(1);
  trk_eta_lim_high = trk_wt->GetXaxis()->GetBinLowEdge(trk_wt_eta_bin);
  trk_phi_lim_high = trk_wt->GetYaxis()->GetBinLowEdge(trk_wt_phi_bin);
  trk_eta_width = (trk_eta_lim_high-trk_eta_lim_low)/trk_wt_eta_bin;
  trk_phi_width = (trk_phi_lim_high-trk_phi_lim_low)/trk_wt_phi_bin;

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis ::execute()
{
  clearVector();
  int rem = -1; //remnant for selecting subsamples
  //bool is_lightTemp = false;
  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
  m_EventNumber = eventInfo->eventNumber();
  //m_RunNumber = eventInfo->runNumber();
  //m_LumiBlock = eventInfo->lumiBlock();
  m_FCal_Et = -99;

  // bool isMC = true;
  // check if the event is MC
  // if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
  //    isMC = true; // can do something with this later
  //ANA_MSG_INFO ("IS MC IS_SIMULATION flag is right");
  // }

  //if (m_isMC)
  //{
  //m_mcEventNumber = eventInfo->mcEventNumber();
  //m_mcChannelNumber = eventInfo->mcChannelNumber();
  //m_mcEventWeight = eventInfo->mcEventWeight();
  //m_mcSumWight += m_mcEventWeight;
  //}

  // if data check if event passes GRL
  if (!m_isMC)
  { // it's data!
    if (!m_grl->passRunLB(*eventInfo))
    {
      ANA_MSG_INFO("drop event: GRL");
      return StatusCode::SUCCESS; // go to next event
    }

    //commented out, barrel LAr not saved
    /*if ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
        (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
        (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ||
        (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)))
    {
      ANA_MSG_INFO ("execute(): eventinfo error");
      return StatusCode::SUCCESS; // go to the next event
    }  */
    // end if event flags check

    // check primary vertex, at least one has to exist
    const xAOD::VertexContainer *pv_container = 0;
    ANA_CHECK(evtStore()->retrieve(pv_container, "PrimaryVertices"));
    unsigned int nPriVx = 0;
    for (auto vert : *pv_container)
    {
      if (vert->vertexType() == xAOD::VxType::PriVtx)
      {
        //m_Vtx_x.push_back(vert->x());
        //m_Vtx_y.push_back(vert->y());
        //m_Vtx_z.push_back(vert->z());
        nPriVx++;
      }
    }
    if (!(nPriVx > 0))
      return StatusCode::SUCCESS;

  } // end if not MC

  // Saving event plane variables
  //if(!m_isMC) EventPlaneAnalysis();
  // only to be used for PbPb data

  const xAOD::HIEventShapeContainer *hiue(0);
  ANA_CHECK(evtStore()->retrieve(hiue, "CaloSums"));
  double m_fcalEt = hiue->at(5)->et();
  m_FCal_Et = m_fcalEt * 1e-6;
  if (m_nsubs == 0)
  {
    rem = 0;
  }
  else
  {
    rem = m_EventNumber % m_nsubs + 1;
  }
  if (!m_isMC)
  {
    // ZDC calibration, it must be done before retrieve ZdcSums
    m_zdcAnalysisTool->reprocessZdc();
    // retrieving ZdcSums
    const xAOD::ZdcModuleContainer *m_zdcSums = 0;
    ANA_CHECK(evtStore()->retrieve(m_zdcSums, "ZdcSums" + auxSuffix));

    bool isCalib = 1;
    double ZdcE = 0;
    for (const xAOD::ZdcModule *zdcModule : *m_zdcSums)
    {
      if (zdcModule->type() != 0)
        continue;

      if (!(zdcModule->isAvailable<float>("CalibEnergy")))
      {
        isCalib = 0;
        continue;
      }
      float modE = zdcModule->auxdecor<float>("CalibEnergy");
      ZdcE += modE;
    }
    if (!isCalib)
    {
      ANA_MSG_INFO("ZDC Module not calibrated");
      return StatusCode::SUCCESS;
    }
    //ANA_MSG_INFO ("ZDC energy sum "<< ZdcE << "Fcal" << m_FCal_Et);

    // Getting number of tracks for the out-of-time pileup
    double m_Num_tracks = 0;
    const xAOD::TrackParticleContainer *tracks = 0;
    ANA_CHECK(evtStore()->retrieve(tracks, "InDetTrackParticles"));
    for (auto Track : *tracks)
    {
      if (m_trkSelection_pup->accept(Track))
        m_Num_tracks++;
    }

    m_is_pileup = m_hiPileup->is_pileup(*hiue, *m_zdcSums); // SAVE in-time pileup Decision HERE 0 = NO pileup, 1 = pileup
    //m_is_oo_pileup = m_hiPileup->is_Outpileup(*hiue, m_Num_tracks); // SAVE out-of-time pileup Decision HERE 0 = NO pileup, 1 = pileup
    evt_fcal[0]->Fill(m_FCal_Et);
    if (m_nsubs > 0)
    {
      evt_fcal_pu[rem]->Fill(m_FCal_Et);
    }
  } //end if not mc
  else
  {
    m_is_pileup = 0;
    //m_is_oo_pileup = 0;
  }
  if (m_is_pileup)
  {
    evt_fcal_pu[0]->Fill(m_FCal_Et);
    if (m_nsubs > 0)
    {
      evt_fcal_pu[rem]->Fill(m_FCal_Et);
    }
    return StatusCode::SUCCESS;
  }
  else
    evt_fcal_npu[0]->Fill(m_FCal_Et);
  if (m_nsubs > 0)
  {
    evt_fcal_npu[rem]->Fill(m_FCal_Et);
  }

  /*if (!m_isMC)
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup(".*");
    for (auto &trig : chainGroup->getListOfTriggers())
    {
      auto cg = m_trigDecisionTool->getChainGroup(trig);
      if (cg->isPassed())
      {
        std::string thisTrig = trig;
        //Info( "execute()", "Event %d: %30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", m_EventNumber, thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );

        std::string thisTrig = trig;
        m_TriggerObject_Chain.push_back(thisTrig);
        m_TriggerObject_Ps.push_back(cg->getPrescale());
      }
    }
  }*/
  int central = -1;

  for (int f = 0; f < (centN - 1); f++)
  {
    if (m_FCal_Et >= CENTCUT[f] && m_FCal_Et < CENTCUT[f + 1])
    {
      central = centN - 2 - f;
    }
  }
  if (m_FCal_Et > CENTCUT[centN - 1])
  {
    ANA_MSG_INFO("Fcal greater than largest bin, dropping event");
    return StatusCode::SUCCESS;
  }
  if (m_FCal_Et < CENTCUT[0])
  {
    if (verbose)
      ANA_MSG_INFO("Fcal: " << m_FCal_Et);
    central = centN - 1; //overflow
  }
  int central_bin = -1;

  for (int c = 0; c < cms_ctr_N; c++)
  {
    if (central >= cms_ctr_range[c] && central < cms_ctr_range[c + 1])
      central_bin = c;
  }
  if (central_bin < 0 || central_bin > (cms_ctr_N - 1))
  {
    ANA_MSG_INFO("wrong central_bin" << central_bin << "; central: " << central << "; Fcal: " << m_FCal_Et);
    return StatusCode::SUCCESS;
  }
  if (verbose)
  {
    ANA_MSG_INFO("Start track loop");
    ANA_MSG_INFO("Fcal bin: " << central << "; central bin: " << central_bin);
  }
  const xAOD::TrackParticleContainer *tracks = 0;
  ANA_CHECK(evtStore()->retrieve(tracks, "InDetTrackParticles"));
  int t = 0;
  if (m_jL)
  {
    double highst = 0.;
    for (auto Track : *tracks)
    {
      if (Track->pt() / 1e3 > highst)
      {
        highst = Track->pt() / 1e3;
      }
    }
    if (verbose)
    {
      ANA_MSG_INFO("Highest track pt:" << highst);
    }

    highst_trk_pt[0]->Fill(highst);
    highst_pt_ctrl[0]->Fill(central, highst);

    if (m_nsubs > 0)
    {
      highst_trk_pt[rem]->Fill(highst);
      highst_pt_ctrl[rem]->Fill(central, highst);
    }

    if (highst < m_jB)
    {
      return StatusCode::SUCCESS;
    }
  }
  evt_fcal_sel[0]->Fill(m_FCal_Et);
  if (m_nsubs > 0)
  {
    evt_fcal_sel[rem]->Fill(m_FCal_Et);
  }
  for (auto Track : *tracks)
  {
    if (!m_trkSelection->accept(*Track))
      continue;
    if (Track->pt() / 1e3 < 0.5)
      continue;
    if (fabs(Track->eta()) > m_refeta)
      continue;
    trk[0]->Fill(Track->eta(), Track->phi());
    trks[central_bin][0]->Fill(Track->eta(), Track->phi());
    trk_eta[0]->Fill(Track->eta());
    trk_phi[0]->Fill(Track->phi());
    trk_etas[central_bin][0]->Fill(Track->eta());
    trk_phis[central_bin][0]->Fill(Track->phi());
    trk_pt[0]->Fill(Track->pt() / 1e3);
    trk_pts[central_bin][0]->Fill(Track->pt() / 1e3);
    if (m_nsubs > 0)
    {
      trk[rem]->Fill(Track->eta(), Track->phi());
      trks[central_bin][rem]->Fill(Track->eta(), Track->phi());
      trk_eta[rem]->Fill(Track->eta());
      trk_phi[rem]->Fill(Track->phi());
      trk_etas[central_bin][rem]->Fill(Track->eta());
      trk_phis[central_bin][rem]->Fill(Track->phi());
      trk_pt[rem]->Fill(Track->pt() / 1e3);
      trk_pts[central_bin][rem]->Fill(Track->pt() / 1e3);
    }
    if (verbose)
    {
      ANA_MSG_INFO("Finished filling tracks");
    }
    if (verbose)
      ANA_MSG_INFO("Track: " << t << "; pT: " << Track->pt() / 1e3 << "; phi" << Track->phi());
    int pt_trk_bin = -1; //exclude tracks of < 1 GeV or > 100 GeV
    for (int pt = 0; pt < cms_pT_N; pt++)
    {
      if (Track->pt() / 1e3 > cms_pT_range[pt] && Track->pt() / 1e3 < cms_pT_range[pt + 1])
        pt_trk_bin = pt;
    }
    if (fabs(Track->eta()) > m_poieta) //exclude tracks eta > 1
      pt_trk_bin = -1;
    double wt = trk_wt->GetBinContent((int)(Track->eta()-trk_eta_lim_low)/trk_eta_width+1,(int)(Track->phi()-trk_phi_lim_low)/trk_phi_width+1);
    //if (verbose)
    //  cout << "pt_trk_bin: " << pt_trk_bin << endl;

    //if (verbose)
    //  cout << "pt_r: " << pt_trk_bin << endl;

    //poi particle of interest
    //p(n,1)
    //H(1,1)
    //ref soft particles
    //Q(n,1), Q(2n,2), Q(n,3)
    //S_(1,1), S_(1,2), S_(2,1), S_(1,3), S_(4,1), S_(1,4), S_(2,2), S_(1,3)
    //both: both
    //q(2n,2), q(n,3)
    //s(1,2), s(1,3), s(1,4)
    if (pt_trk_bin >= 0) //if poi
    {
      n_trks_poi[pt_trk_bin] = n_trks_poi[pt_trk_bin] + 1;
      //Q_(n,k)=sum(wt^k*exp(i*n*phi_i)),S_(p,k)=(sum(wt^k))^p qnk(k,phi_n,wt) snk(k,wt)
      //Q_(n,1)
      k = 1;
      N_ = m_N;
      qnk_pt_poi[pt_trk_bin] = qnk_pt_poi[pt_trk_bin] + qnk(N_, k, Track->phi(), wt);
      //if (verbose)
      // cout << "p_(n,1): " << qnk_pt_poi[pt_trk_bin] << endl;

      //H_(1,1)
      k = 1;
      spk_pt_poi[pt_trk_bin] = spk_pt_poi[pt_trk_bin] + snk(k, wt);
      //if (verbose)
      //  cout << "H_(1,1): " << spk_pt_poi[pt_trk_bin] << endl;
    }                             //end poi
    if (Track->pt() / 1e3 <= 3.0) //if ref
    {
      //Q_(n,1)
      n_trks_ref = n_trks_ref + 1;
      k = 1;
      N_ = m_N;
      qnk_pt_ref[0] = qnk_pt_ref[0] + qnk(N_, k, Track->phi(), wt);
      //Q_(2n,2)
      k = 2;
      N_ = 2 * m_N;
      qnk_pt_ref[1] = qnk_pt_ref[1] + qnk(N_, k, Track->phi(), wt);
      //Q_(n,3)
      k = 3;
      N_ = m_N;
      qnk_pt_ref[2] = qnk_pt_ref[2] + qnk(N_, k, Track->phi(), wt);
      //if (verbose)
      //  cout << "Q_(n,1): " << qnk_pt_ref[0] << "; Q_(2n,2): " << qnk_pt_ref[1] << "; Q_(n,3): " << qnk_pt_ref[2] << endl;

      //S_(1,1)
      k = 1;
      spk_pt_ref[0] = spk_pt_ref[0] + snk(k, wt);
      //S_(1,2)
      k = 2;
      spk_pt_ref[1] = spk_pt_ref[1] + snk(k, wt);
      //S_(2,1)
      k = 1;
      spk_pt_ref[2] = spk_pt_ref[2] + snk(k, wt);
      //S_(1,3)
      k = 3;
      spk_pt_ref[3] = spk_pt_ref[3] + snk(k, wt);
      //S_(4,1)
      k = 1;
      spk_pt_ref[4] = spk_pt_ref[4] + snk(k, wt);
      //S_(1,4)
      k = 4;
      spk_pt_ref[5] = spk_pt_ref[5] + snk(k, wt);
      //S_(2,2)
      k = 2;
      spk_pt_ref[6] = spk_pt_ref[6] + snk(k, wt);
      //S_(3,1)
      k = 1;
      spk_pt_ref[7] = spk_pt_ref[7] + snk(k, wt);
      //if (verbose)
      //  cout << "S_(1,1): " << spk_pt_ref[0] << "; S_(1,2): " << spk_pt_ref[1] << "; S_(2,1): " << spk_pt_ref[2] << "; S_(1,3): " << spk_pt_ref[3] << endl;
      //if (verbose)
      //  cout << "S_(4,1): " << spk_pt_ref[4] << "; S_(1,4): " << spk_pt_ref[5] << "; S_(2,2): " << spk_pt_ref[6] << "S_(3,1): " << spk_pt_ref[7] << endl;
    } //end ref

    //both:
    //q(2n,2), q(n,3)
    //s(1,2), s(1,3), s(1,4)
    if (Track->pt() / 1e3 <= 3.0 && pt_trk_bin >= 0) //if both
    {
      //q_(2n,2)
      k = 2;
      N_ = 2 * m_N;
      qnk_pt_both[pt_trk_bin][0] = qnk_pt_both[pt_trk_bin][0] + qnk(N_, k, Track->phi(), wt);
      //q_(n,3)
      k = 3;
      N_ = m_N;
      qnk_pt_both[pt_trk_bin][1] = qnk_pt_both[pt_trk_bin][1] + qnk(N_, k, Track->phi(), wt);
      //if (verbose)
      //  cout << "q_(n,1): " << qnk_pt_both[pt_trk_bin][0] << "; q_(n,3): " << qnk_pt_both[pt_trk_bin][1] << endl;

      //s_(1,2)
      k = 2;
      spk_pt_both[pt_trk_bin][0] = spk_pt_both[pt_trk_bin][0] + snk(k, wt);
      //s_(1,3)
      k = 3;
      spk_pt_both[pt_trk_bin][1] = spk_pt_both[pt_trk_bin][1] + snk(k, wt);
      //s_(1,4)
      k = 4;
      spk_pt_both[pt_trk_bin][2] = spk_pt_both[pt_trk_bin][2] + snk(k, wt);
      //if (verbose)
      //  cout << "s_(1,2): " << spk_pt_both[pt_trk_bin][0] << "; s_(1,3): " << spk_pt_both[pt_trk_bin][1] << "; s_(1,4): " << spk_pt_both[pt_trk_bin][2] << endl;
    }
    t++;
  } //end track loop
  if (verbose)
    ANA_MSG_INFO("End Track loop, beginning computation");
  /*if (report)
  {
    cout << "..........." << endl;
    cout << "Results comparison:" << endl;
    cout << "Centrality: " << central << endl;
  }*/

  for (int pt = 0; pt < cms_pT_N; pt++)
  {
    /*if (report)
    {
      cout << endl;
      cout << "pt ind: " << pt << endl;
    }*/
    //poi particle of interest
    //p(n,1)
    //H(1,1)
    //ref soft particles
    //Q(n,1), Q(2n,2), Q(n,3)
    //S_(1,1), S_(1,2), S_(2,1), S_(1,3), S_(4,1), S_(1,4), S_(2,2)
    //both: both
    //q(2n,2), q(n,3)
    //s(1,2), s(1,3), s(1,4)
    //poi
    //H_(1,1)
    p = 1;
    spk_pt_poi[pt] = pow(spk_pt_poi[pt], p);
    if (pt == 0)
    {
      //ref
      //S_(1,1)
      p = 1;
      spk_pt_ref[0] = pow(spk_pt_ref[0], p);
      //S_(1,2)
      p = 1;
      spk_pt_ref[1] = pow(spk_pt_ref[1], p);
      //S_(2,1)
      p = 2;
      spk_pt_ref[2] = pow(spk_pt_ref[2], p);
      //S_(1,3)
      p = 1;
      spk_pt_ref[3] = pow(spk_pt_ref[3], p);
      //S_(4,1)
      p = 4;
      spk_pt_ref[4] = pow(spk_pt_ref[4], p);
      //S_(1,4)
      p = 1;
      spk_pt_ref[5] = pow(spk_pt_ref[5], p);
      //S_(2,2)
      p = 2;
      spk_pt_ref[6] = pow(spk_pt_ref[6], p);
      //S_(3,1)
      p = 3;
      spk_pt_ref[7] = pow(spk_pt_ref[7], p);
    }

    //both
    //s_(1,2)
    p = 1;
    spk_pt_both[pt][0] = pow(spk_pt_both[pt][0], p);
    //s_(1,3)
    p = 1;
    spk_pt_both[pt][1] = pow(spk_pt_both[pt][1], p);
    //s_(1,4)
    p = 1;
    spk_pt_both[pt][2] = pow(spk_pt_both[pt][2], p);

    /*if (verbose)
    {
      //poi
      cout << "p_(n,1): " << qnk_pt_poi[pt] << endl;
      cout << "H_(1,1): " << spk_pt_poi[pt] << endl;
      //ref
      if (pt == 0)
      {
        cout << "Q_(n,1): " << qnk_pt_ref[0] << "; Q_(2n,2): " << qnk_pt_ref[1] << "; Q_(n,3): " << qnk_pt_ref[2] << endl;
        cout << "S_(1,1): " << spk_pt_ref[0] << "; S_(1,2): " << spk_pt_ref[1] << "; S_(2,1): " << spk_pt_ref[2] << "; S_(1,3): " << spk_pt_ref[3] << endl;
        cout << "S_(4,1): " << spk_pt_ref[4] << "; S_(1,4): " << spk_pt_ref[5] << "; S_(2,2): " << spk_pt_ref[6] << "; S_(3,1): " << spk_pt_ref[7] << endl;
      }
      //both
      cout << "q_(n,1): " << qnk_pt_both[pt][0] << "; q_(n,3): " << qnk_pt_both[pt][1] << endl;
      cout << "s_(1,2): " << spk_pt_both[pt][0] << "; s_(1,3): " << spk_pt_both[pt][1] << "; s_(1,4): " << spk_pt_both[pt][2] << endl;
    }*/
    //integrated correlations
    //0:Q_(n,1), 1:Q_(2n,2), 2:Q_(n,3)
    //0:S_(1,1), 1:S_(1,2), 2:S_(2,1), 3:S_(1,3), 4:S_(4,1), 5:S_(1,4), 6: S_(2,2), 7:S_(3,1)
    //c[0]=(Q_(n,1)xQ_(n,1)*-S_(1,2))/(S_(2,1)-S_(1,2))
    //c[1]=((Q_(n,1)xQ_(n,1)*)^2+(Q_(2n,2)xQ_(2n,2)*)-2xRe(Q_(2n,2)xQ_(n,1)*xQ_(n,1)*)+8xRe(Q_(n,3)xQ_(n,1)*)-4xS_(1,2)x(Q_(n,1)xQ_(n,1)*)^2+2xS_(2,2)-6xS_(1,4))/(S_(4,1)+8xS_(1,3)xS_(1,1)-6xS_(1,2)xS(2,1)+3xS_(2,2)-6xS_(1,4))
    if (pt == 0)
    {
      c_wt_pt_ref[0] = spk_pt_ref[2] - spk_pt_ref[1];
      c_wt_pt_ref[1] = spk_pt_ref[4]                       //S_(4,1)
                       + 8 * spk_pt_ref[3] * spk_pt_ref[0] //8S_(1,3)*S_(1,1)
                       - 6 * spk_pt_ref[1] * spk_pt_ref[2] //-6S_(1,2)*S(2,1)
                       + 3 * spk_pt_ref[6]                 //3S_(2,2)
                       - 6 * spk_pt_ref[5];                //-6S_(1,4)
      c_den_pt_ref[0] = (TComplex::Conjugate(qnk_pt_ref[0]) * qnk_pt_ref[0]).Re() - spk_pt_ref[1];
      c_den_pt_ref[1] = TComplex::Power(TComplex::Conjugate(qnk_pt_ref[0]) * qnk_pt_ref[0], 2)              //(Q_(n,1)*Q_(n,1)*)^2
                        + (TComplex::Conjugate(qnk_pt_ref[1]) * qnk_pt_ref[1]).Re()                         //(Q_(2n,2)*Q_(2n,2)*)
                        - 2 * (qnk_pt_ref[1] * TComplex::Power(TComplex::Conjugate(qnk_pt_ref[0]), 2)).Re() //-2Re(Q_(2n,2)*(Q_(n,1)*)^2)
                        + 8 * (qnk_pt_ref[2] * TComplex::Conjugate(qnk_pt_ref[0])).Re()                     //8Re(Q_(n,3)*Q_(n,1)*)
                        - 4 * spk_pt_ref[1] * (TComplex::Conjugate(qnk_pt_ref[0]) * qnk_pt_ref[0]).Re()     //-4S_(1,2)*(Q_(n,1)*Q_(n,1)*)
                        + 2 * spk_pt_ref[6]                                                                 //2S_(2,2)
                        - 6 * spk_pt_ref[5];                                                                //-6S_(1,4)

      /*if (report)
      {
        cout << "Number of Tracks: " << n_trks_ref << endl;
      }*/
      for (int halfk = 0; halfk < maxk / 2; halfk++)
      {
        if (n_trks_ref >= (halfk + 1) * 2)
        {
          if (verbose)
          {
            ANA_MSG_INFO("Filling" << halfk << ";centralbi" << central_bin << "value: " << c_den_pt_ref[halfk] / c_wt_pt_ref[halfk] << "weight" << c_wt_pt_ref[halfk]);
          }
          cs[halfk][central_bin][0]->Fill(c_den_pt_ref[halfk] / c_wt_pt_ref[halfk], c_wt_pt_ref[halfk]);
          if (m_nsubs > 0)
          {
            cs[halfk][central_bin][rem]->Fill(c_den_pt_ref[halfk] / c_wt_pt_ref[halfk], c_wt_pt_ref[halfk]);
          }
          //loop over all possible combinations
          //co2 > co
          if (m_isCov)
          {
            for (int co = 0; co < halfk; co++)
            {
              cov[co][halfk][pt][central_bin]->Fill(c_den_pt_ref[co] / c_wt_pt_ref[co] * c_den_pt_ref[halfk] / c_wt_pt_ref[halfk], c_wt_pt_ref[co] * c_wt_pt_ref[halfk]);
              if (m_nsubs > 0)
              {
                cov[co][halfk][pt][central_bin]->Fill(c_den_pt_ref[co] / c_wt_pt_ref[co] * c_den_pt_ref[halfk] / c_wt_pt_ref[halfk], c_wt_pt_ref[co] * c_wt_pt_ref[halfk]);
              }
              /*if (report)
            {
              cout << "pT: " << pt << endl;
              cout << "expectation of: c" << (co + 1) * 2 << " and c" << (halfk + 1) * 2 << ": " << c_den_pt_ref[co] / c_wt_pt_ref[co] * c_den_pt_ref[halfk] / c_wt_pt_ref[halfk] << endl;
              cout << "Histo filled: " << cov[co][halfk][pt][central_bin]->GetName() << endl;
              cout << "Weight: c" << (co + 1) * 2 << "*c" << (halfk + 1) * 2 << ": " << c_wt_pt_ref[co] << "*" << c_wt_pt_ref[pt] << "=" << c_wt_pt_ref[co] * c_wt_pt_ref[halfk] << endl;
            }*/
            }
          }
        }
        else
        {
          c_den_pt_ref[halfk] = 0.;
          c_wt_pt_ref[halfk] = 0.;
        }
      } //end halfk loop
    }   //end pt=0 loop

    //pt differential corr
    //poi particle of interest
    //p(n,1)
    //H(1,1)
    //ref soft particles
    //0:Q_(n,1), 1:Q_(2n,2), 2:Q_(n,3)
    //0:S_(1,1), 1:S_(1,2), 2:S_(2,1), 3:S_(1,3), 4:S_(4,1), 5:S_(1,4), 6:S_(2,2), 7:S_(3,1)
    //both: both
    //0: q(2n,2), 1: q(n,3)
    //0: s(1,2), 1: s(1,3), 2: s(1,4)
    //poi
    //H_(1,1)
    //c[0](pt)=(Re(p_(n,1)(pt)xQ_(n,1)^*)-s_(1,2)(pt))/(H_(1,1)(pt)xS_(1,1)-s_(1,2)(pt)))
    c_den_pt_diff[0][pt] = (qnk_pt_poi[pt] * TComplex::Conjugate(qnk_pt_ref[0])).Re() //Re(p_(n,1)(pt)xQ_(n,1)^*)
                           - spk_pt_both[pt][0];                                      //-s_(1,2)(pt)
    c_wt_pt_diff[0][pt] = spk_pt_poi[pt] * spk_pt_ref[0] - spk_pt_both[pt][0];        //(H_(1,1)(pt)xS_(1,1)-s_(1,2)(pt)))
    //c[1](pt)=(Re(p(n,1)xQ(n,1)xQ(n,1)*xQ(n,1)*-q(2n,2)xQ(n,1)*xQ(n,1)*-p(n,1)xQ(n,1)xQ(2n,2)*-2xS(1,2)xp(n,1)xQ(n,1)*-2xs(1,2)xQ(n,1)xQ(n,1)*
    //+7xq(n,3)xQ(n,1)*-Q(n,1)xq(n,3)*+q(2n,2)xQ(2n,2)*+2xp(n,1)xQ(n,3)*+2xs(1,1)xS(1,2)-6xs(1,4)))/
    //(H(1,1)x(S(3,1)-3xS(1,1)xS(1,2)+2xS(1,3))-3x(s(1,2)x(S(2,1)-S(1,2))+2x(s(1,4)-s(1,3)xS(1,1))))
    c_den_pt_diff[1][pt] = (qnk_pt_poi[pt] * qnk_pt_ref[0] * TComplex::Conjugate(qnk_pt_ref[0]) * TComplex::Conjugate(qnk_pt_ref[0])).Re()              //p(n,1)xQ(n,1)xQ(n,1)*xQ(n,1)*
                           - (qnk_pt_both[pt][0] * TComplex::Conjugate(qnk_pt_ref[0]) * TComplex::Conjugate(qnk_pt_ref[0])).Re()                        //-q(2n,2)xQ(n,1)*xQ(n,1)*
                           - (qnk_pt_poi[pt] * qnk_pt_ref[0] * TComplex::Conjugate(qnk_pt_ref[1])).Re()                                                 //-p(n,1)xQ(n,1)xQ(2n,2)*
                           - 2 * spk_pt_ref[1] * (qnk_pt_poi[pt] * TComplex::Conjugate(qnk_pt_ref[0])).Re()                                             //-2xS(1,2)xp(n,1)xQ(n,1)*
                           - 2 * spk_pt_both[pt][0] * (qnk_pt_ref[0] * TComplex::Conjugate(qnk_pt_ref[0])).Re()                                         //-2xs(1,2)xQ(n,1)xQ(n,1)*
                           + 7 * (qnk_pt_both[pt][1] * TComplex::Conjugate(qnk_pt_ref[0])).Re()                                                         //+7xq(n,3)xQ(n,1)*
                           - (qnk_pt_ref[0] * TComplex::Conjugate(qnk_pt_both[pt][1])).Re()                                                             //-Q(n,1)xq(n,3)*
                           + (qnk_pt_both[pt][0] * TComplex::Conjugate(qnk_pt_ref[1])).Re()                                                             //+q(2n,2)xQ(2n,2)*
                           + 2 * (qnk_pt_poi[pt] * TComplex::Conjugate(qnk_pt_ref[2])).Re()                                                             //+2xp(n,1)xQ(n,3)*
                           + 2 * spk_pt_both[pt][0] * spk_pt_ref[1]                                                                                     //+2xs(1,1)xS(1,2)
                           - 6 * spk_pt_both[pt][2];                                                                                                    //-6xs(1,4)));
    c_wt_pt_diff[1][pt] = spk_pt_poi[pt] * (spk_pt_ref[7] - 3 * spk_pt_ref[0] * spk_pt_ref[1] + 2 * spk_pt_ref[3])                                      //H(1,1)x(S(3,1)-3xS(1,1)xS(1,2)+2xS(1,3))
                          - 3 * (spk_pt_both[pt][0] * (spk_pt_ref[2] - spk_pt_ref[1]) + 2 * (spk_pt_both[pt][2] - spk_pt_both[pt][1] * spk_pt_ref[0])); //-3x(s(1,2)x(S(2,1)-S(1,2))+2x(s(1,4)-s(1,3)xS(1,1)));

    for (int halfk = 0; halfk < maxk / 2; halfk++)
    {
      if (n_trks_poi[pt] >= 1 && n_trks_ref >= ((halfk + 1) * 2 - 1))
      {
        if (verbose)
          ANA_MSG_INFO("Filling diff" << halfk << ";centralbi" << central_bin << ";pt" << pt << ";value: " << c_den_pt_ref[halfk] / c_wt_pt_ref[halfk] << "weight" << c_wt_pt_ref[halfk]);
        c_diffs[halfk][pt][central_bin][0]->Fill(c_den_pt_diff[halfk][pt] / c_wt_pt_diff[halfk][pt], c_wt_pt_diff[halfk][pt]);
        if (m_nsubs > 0)
        {
          c_diffs[halfk][pt][central_bin][rem]->Fill(c_den_pt_diff[halfk][pt] / c_wt_pt_diff[halfk][pt], c_wt_pt_diff[halfk][pt]);
        }
        //loop over all possible combinations
        //correlation with c_2k
        if (m_isCov)
        {
          for (int co = 0; co < maxk / 2; co++)
          {
            if (n_trks_ref >= (co + 1) * 2)
            {
              cov[co][halfk + maxk / 2][pt][central_bin]->Fill(c_den_pt_ref[co] / c_wt_pt_ref[co] * c_den_pt_diff[halfk][pt] / c_wt_pt_diff[halfk][pt], c_wt_pt_ref[co] * c_wt_pt_diff[halfk][pt]);
              /*if (report)
            {
              cout << "pT: " << pt << endl;
              cout << "expectation of: c" << (co + 1) * 2 << " and d" << (halfk + 1) * 2 << ": " << c_den_pt_ref[co] / c_wt_pt_ref[co] * c_den_pt_diff[halfk][pt] / c_wt_pt_diff[halfk][pt] << endl;
              cout << "Histo filled: " << cov[co][halfk + maxk / 2][pt][central_bin]->GetName() << endl;
              cout << "Weight: c" << (co + 1) * 2 << "*d" << (halfk + 1) * 2 << ": " << c_wt_pt_ref[co] << "*" << c_wt_pt_diff[halfk][pt] << "=" << c_wt_pt_ref[co] * c_wt_pt_diff[halfk][pt] << endl;
            }*/
            }
          }

          //correlation with d_2k
          for (int co = 0; co < halfk; co++)
          {
            cov[co + maxk / 2][halfk + maxk / 2][pt][central_bin]->Fill(c_den_pt_diff[co][pt] / c_wt_pt_diff[co][pt] * c_den_pt_diff[halfk][pt] / c_wt_pt_diff[halfk][pt], c_wt_pt_diff[co][pt] * c_wt_pt_diff[halfk][pt]);
            /*if (report)
          {
            cout << "pT: " << pt << endl;
            cout << "expectation of: d" << (co + 1) * 2 << " and d" << (halfk + 1) * 2 << ": " << c_den_pt_diff[co][pt] / c_wt_pt_diff[co][pt] * c_den_pt_diff[halfk][pt] / c_wt_pt_diff[halfk][pt] << endl;
            cout << "Histo filled: " << cov[co + maxk / 2][halfk + maxk / 2][pt][central_bin]->GetName() << endl;
            cout << "Weight: d" << (co + 1) * 2 << "*d" << (halfk + 1) * 2 << ": " << c_wt_pt_diff[co][pt] << "*" << c_wt_pt_diff[halfk][pt] << "=" << c_wt_pt_diff[co][pt] * c_wt_pt_diff[halfk][pt] << endl;
          }*/
          }
        }
      }
      else
      {
        c_den_pt_diff[halfk][pt] = 0.;
        c_wt_pt_diff[halfk][pt] = 0.;
      }
    } //end halfk loop

    /*if (report)
    {
      if (pt == 0)
      {
        cout << "r2 wt: " << f2wt << endl;
        cout << "r2: " << r2 / f2wt << endl;
        cout << "central: " << central_bin << endl;
        cout << "c2_wt: " << c_wt_pt_ref[0] << endl;
        cout << "c2: " << c_den_pt_ref[0] / c_wt_pt_ref[0] << endl;
      }
      cout << "pt: " << pt << "; central: " << central_bin << endl;
      cout << "d2_wt: " << c_wt_pt_diff[0][pt] << endl;
      cout << "d2: " << c_den_pt_diff[0][pt] / c_wt_pt_diff[0][pt] << endl;
      cout << "b2 wt: " << evtwt2[pt] << endl;
      cout << "b2: " << b2[pt] / evtwt2[pt] << endl;
      if (pt == 0)
      {
        cout << "r4 wt: " << f4wt << endl;
        cout << "r4: " << r4 / f4wt << endl;
        cout << "central: " << central_bin << endl;
        cout << "c4_wt: " << c_wt_pt_ref[1] << endl;
        cout << "c4: " << c_den_pt_ref[1] / c_wt_pt_ref[1] << endl;
      }
      cout << "pt: " << pt << "; central: " << central_bin << endl;
      cout << "d4_wt: " << c_wt_pt_diff[1][pt] << endl;
      cout << "d4: " << c_den_pt_diff[1][pt] / c_wt_pt_diff[1][pt] << endl;
      cout << "b4 wt: " << evtwt4[pt] << endl;
      cout << "b4: " << b4[pt] / evtwt4[pt] << endl;
    }*/
  } //end pt loop

  // }

  //if (!m_isMC && !m_Track_pt.empty())
  //  m_tree->Fill();

  //Muon deleted, for now
  //if (!m_isMC && !m_Muon_pt.empty()) m_tree->Fill();
  //if (m_isMC && (!m_Jet2_th_pt.empty() || !m_Jet4_th_pt.empty())) m_tree->Fill();
  //if(m_isMC && !m_partonB.empty()) m_tree->Fill();
  //if(m_isMC && (!m_Muon_pt.empty() || !m_TruthMuon_pt.empty()) ) m_tree->Fill();
  //if(m_isMC && (m_Muon_pt.empty() && m_TruthMuon_pt.empty()) ) m_tree->Fill();
  //if(!m_isMC && m_Muon_pt.empty() ) m_tree->Fill();
  //if(!m_Muon_pt.empty()) m_tree->Fill();

  //if(!m_minbias && !m_doTrk && !m_Muon_pt.empty()) m_tree->Fill();
  //if(!m_Jet_pt.empty()) m_tree->Fill();
  //if(!m_minbias && m_doTrk && m_Muon_pt.empty() && !m_Jet2_pt.empty() && !m_Track_pt.empty()) m_tree->Fill();
  //clearVector();
  //m_tree->Fill();

  return StatusCode::SUCCESS;
}

void MyxAODAnalysis ::clearVector()
{
  for (int halfk = 0; halfk < maxk / 2; halfk++)
  {
    c_den_pt_ref[halfk] = 0.;
    c_wt_pt_ref[halfk] = 0.;
  }

  n_trks_ref = 0;
  //ref
  for (int Q = 0; Q < qnks_ref; Q++) //Q_(n,1), Q_(2n,2), Q_(n,3)
  {
    qnk_pt_ref[Q] = a;
  }
  for (int S = 0; S < spks_ref; S++) //S_(1,1), S_(1,2), S_(2,1), S_(1,3), S_(4,1), S_(1,4), S_(2,2)
  {
    spk_pt_ref[S] = 0.;
  }
  for (int pt = 0; pt < cms_pT_N; pt++)
  {
    //poi
    qnk_pt_poi[pt] = a;
    spk_pt_poi[pt] = 0.;
    //both
    for (int Q = 0; Q < qnks_both; Q++) //q(2n,2), q(n,3)
    {
      qnk_pt_both[pt][Q] = a;
    }
    for (int S = 0; S < spks_both; S++) //s(1,2), s(1,3), s(1,4)
    {
      spk_pt_both[pt][S] = 0.;
    }
    for (int halfk = 0; halfk < maxk / 2; halfk++)
    {
      c_den_pt_diff[halfk][pt] = 0.;
      c_wt_pt_diff[halfk][pt] = 0.;
    }
    n_trks_poi[pt] = 0;
  }

  /*m_Vtx_x.clear();
    m_Vtx_y.clear();
    m_Vtx_z.clear();

    m_Track_pt.clear();
    m_Track_eta.clear();
    m_Track_phi.clear();
    m_Track_charge.clear();
    m_Track_d0.clear();
    m_Track_z0.clear();
    m_Track_theta.clear();

    m_Muon_pt.clear();
  m_Muon_eta.clear();
  m_Muon_phi.clear();
  m_Muon_charge.clear();
  m_Muon_eloss.clear();
  m_Muon_eloss_type.clear();
  m_Muon_quality.clear();
  m_Muon_e.clear();
  m_Muon_dRj.clear();
  m_Muon_id_pt.clear();
  m_Muon_id_eta.clear();
  m_Muon_id_phi.clear();
  m_Muon_id_theta.clear();
  m_Muon_id_d0.clear();
  m_Muon_id_z0.clear();
  m_Muon_TrigMatch3.clear();
  m_Muon_TrigMatch4.clear();
  m_Muon_parent.clear();
  m_Muon_truth_pt.clear();
  m_Muon_truth_eta.clear();
  m_Muon_truth_phi.clear();

  m_TruthMuon_pt.clear();
  m_TruthMuon_eta.clear();
  m_TruthMuon_phi.clear();
  m_TruthMuon_charge.clear();
  m_TruthMuon_e.clear();

  m_Muon_cluster_et.clear();
  m_Muon_cluster_pt.clear();
  m_Muon_cluster_e.clear();
  m_Muon_cluster_eta.clear();
  m_Muon_cluster_phi.clear();

    m_TriggerObject_Chain.clear();
    m_TriggerObject_Ps.clear();*/
  //m_partonB.clear();
  //m_partonC.clear();
  //m_partonT.clear();
  //m_partonN.clear();
}

double MyxAODAnalysis::dR(const double eta1,
                          const double phi1,
                          const double eta2,
                          const double phi2)
{
  double deta = fabs(eta1 - eta2);
  double dphi = fabs(phi1 - phi2) < TMath::Pi() ? fabs(phi1 - phi2) : 2 * TMath::Pi() - fabs(phi1 - phi2);
  return sqrt(deta * deta + dphi * dphi);
}

double MyxAODAnalysis::snk(int k, double weight)
{
  return pow(weight, k);
}
TComplex MyxAODAnalysis::qnk(int n, int k, double angle, double weight)
{
  return TComplex::Exp(I * (double)(n)*angle) * (double)pow(weight, k);
}

StatusCode MyxAODAnalysis ::finalize()
{
  ANA_MSG_INFO("in finalize");
  //ANA_MSG_INFO("finalize(): sum total MC weight = " << m_mcSumWight);
  m_th1->AddBinContent(1, m_mcSumWight);
  //time_t my_time = time(NULL);

  // ctime() used to give the present time
  //printf("%s", ctime(&my_time));

  return StatusCode::SUCCESS;
}
