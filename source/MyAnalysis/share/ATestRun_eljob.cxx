void ATestRun_eljob (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = "/afs/cern.ch/work/x/xiaoning/public/muon_perf/mc16_5TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.merge.AOD.e4973_d1521_r11472_r11217/";
  //SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);
  const char* inputFilePath = "/afs/cern.ch/work/x/xiaoning/public/muon_perf/mc16_5TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.recon.AOD.e4973_s3238_r11199/";
  SH::ScanDir().filePattern("AOD.17531913._005973.pool.root.1").scan(sh.inputFilePath);


  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  // job.options()->setDouble (EL::Job::optMaxEvents, 100); // for testing purposes, limit to run over the first 500 events only!
  job.outputAdd( EL::OutputStream( "myOutput"));

  // add our algorithm to the job
  EL::AnaAlgorithmConfig alg;
  alg.setType ("MuonxAODPerf");

  // set the name of the algorithm (this is the name use with
  // messages)
  alg.setName ("AnalysisAlg");
  alg.setProperty("Verbose",false).ignore();
  alg.setProperty("Cut",false).ignore();


  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submit (job, submitDir);
} 
