//#include <SampleHandler/ToolsDiscovery.h>

void ATestSubmit(const std::string &submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData/r9315/");
  // SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);
  // SH::scanRucio (sh, "mc16_5TeV.42001*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*R04.recon.AOD.e*_s3238_r11199/");
SH::scanRucio(sh, "data18_hi.00366919.calibration_PCPEB.merge.AOD.k1029_m2048");
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString("nc_tree", "CollectionTree");
  sh.setMetaString("nc_grid_filter", "*AOD*");
  // further sample handler configuration may go here

  // print out the samples we found
  sh.print();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler(sh); // use SampleHandler in this job
  // job.options()->setDouble (EL::Job::optMaxEvents, 100); // for testing purposes, limit to run over the first 100 events only!
  // Add an output stream called "ANALYSIS".
  job.outputAdd(EL::OutputStream("myOutput"));
  // add our algorithm to the job
  EL::AnaAlgorithmConfig alg;
  alg.setType("MyxAODAnalysis");

  // set the name of the algorithm (this is the name use with
  // messages)
  alg.setName("AnalysisAlg");
  //alg.setProperty( "isPp", true ).ignore();
  alg.setProperty("isMC", false).ignore();
  alg.setProperty("N", 3).ignore();
  alg.setProperty("Verbose", false).ignore();
  alg.setProperty("RefEta", 2.5).ignore();
  alg.setProperty("PoiEta", 2.5).ignore();
  alg.setProperty("isCov", true).ignore();
  alg.setProperty("numSubs", 0).ignore();
  alg.setProperty("jumpLow", true).ignore();
  alg.setProperty("jumpBar", 20.0).ignore();

  // alg.setProperty( "Verbose", false).ignore();
  //alg.setProperty("Cut",false).ignore();
  // later on we'll add some configuration options for our algorithm that go here

  //add the GRL tool to the algorithm

  job.algsAdd(alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.xiaoning.test-grid-0806-1.%in:name[2]%.%in:name[3]%.%in:name[6]%");
  // we can use other drivers to run things on the Grid, with PROOF, etc.
  driver.options()->setDouble("nc_nFilesPerJob", 4);

  job.options()->setString(EL::Job::optSubmitFlags, " --nGBPerJob=MAX");

  // process the job using the driver
  driver.submitOnly(job, submitDir);
}
